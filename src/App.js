import React from 'react';
import './App.scss';
import Did from './sections/Did';
import Do from './sections/Do';
import { Here } from './sections/Here';
import Main from './sections/Main';

import ScrollableContainer from "react-full-page-scroll";
import classNames from "classnames"

const PageComponent = ({ children }) => {
  return (<div>{children}</div>)
}

const Cursor = () => {
  const [position, setPosition] = React.useState({ x: 0, y: 0 });
  const [clicked, setClicked] = React.useState(false);
  const [linkHovered, setLinkHovered] = React.useState(false);
  const [hidden, setHidden] = React.useState(false);

  React.useEffect(() => {
    addEventListeners();
    handleLinkHoverEvents();
    return () => removeEventListeners();
  }, []);

  const addEventListeners = () => {
    document.addEventListener("mousemove", onMouseMove);
    document.addEventListener("mouseenter", onMouseEnter);
    document.addEventListener("mouseleave", onMouseLeave);
    document.addEventListener("mousedown", onMouseDown);
    document.addEventListener("mouseup", onMouseUp);
  };

  const removeEventListeners = () => {
    document.removeEventListener("mousemove", onMouseMove);
    document.removeEventListener("mouseenter", onMouseEnter);
    document.removeEventListener("mouseleave", onMouseLeave);
    document.removeEventListener("mousedown", onMouseDown);
    document.removeEventListener("mouseup", onMouseUp);
  };

  const onMouseMove = (e) => {
    setPosition({ x: e.clientX, y: e.clientY });
  };

  const onMouseDown = () => {
    setClicked(true);
  };

  const onMouseUp = () => {
    setClicked(false);
  };

  const onMouseLeave = () => {
    setHidden(true);
  };

  const onMouseEnter = () => {
    setHidden(false);
  };

  const handleLinkHoverEvents = () => {
    document.querySelectorAll("a, button").forEach((el) => {
      el.addEventListener("mouseover", () => setLinkHovered(true));
      el.addEventListener("mouseout", () => setLinkHovered(false));
    });
  };

  const cursorClasses = classNames("cursor", {
    "cursor--clicked": clicked,
    "cursor--hidden": hidden,
    "cursor--link-hovered": linkHovered
  });

  return (
    <div
      className={cursorClasses}
      style={{ left: `${position.x}px`, top: `${position.y}px` }}
    >
      D.</div>
  );
}

function App() {
  return (
    <>
      <Cursor />
      <ScrollableContainer animationTime={1400}>
        <PageComponent>
          <Main />
        </PageComponent>
        <PageComponent>
          <Do />
        </PageComponent>
        <PageComponent>
          <Did />
        </PageComponent>
        <PageComponent>
          <Here />
        </PageComponent>
      </ScrollableContainer>
    </>
  );
}

export default App;
