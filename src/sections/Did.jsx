import React from 'react'
import '../styles/Did.scss'

import WorkPic from "../assets/hero4.jpg"

const Did = () => {
    return (
        <section className="Did-wrapper">
            <div className="Did-left">
                <h2>エウ ドメニカ。DOM</h2>
                <p>
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
                    eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam
                    voluptua. At vero eos et accusam et justo duo dolores et ea
                    rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum
                    dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr
                    , sed diam nonumy eirmod tempor invidunt ut labore et
                    dolore magna aliquyam erat, sed diam voluptua. At vero eos et
                    accusam et justo duo
                </p>
            </div>
            <div className="Did-right">
                <div className="Did-image">
                    <img src={WorkPic} alt=""/>
                </div>
            </div>
        </section>
    )
}

export default Did
