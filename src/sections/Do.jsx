import React from 'react'
import '../styles/Do.scss'

const Do = () => {
    return (
        <section className="D-wrapper">
            <div className="D-header">
                <h2>What we do</h2>
                <a href="/"><p>02.Do</p></a>
            </div>
            <div className="D-content">
                <div className="D-content-top">
                    <div className="D-content-container">
                        <div className="D-content-container-frame" />
                        <h3 className="D-content-container-title">Graphic Design</h3>
                        <p className="D-content-container-text">
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr
                            , sed diam nonumy eirmod tempor invidunt ut labore et dolore
                            magna aliquyam erat, sed diam voluptua. At vero eos
                            et accusam et justo duo dolores et ea rebum.
                            Stet clita kasd gubergren, no sea takimata
                        </p>
                    </div>
                    <div className="D-content-container">
                        <div className="D-content-container-frame" />
                        <h3 className="D-content-container-title">Branding</h3>
                        <p className="D-content-container-text">
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr
                            , sed diam nonumy eirmod tempor invidunt ut labore et dolore
                            magna aliquyam erat, sed diam eos
                            et accusam et justo duo dolores et ea rebum.
                            Stet clita kasd gubergren, no sea takimata
                        </p>
                    </div>
                </div>
                <div className="D-content-bottom">
                    <div className="D-content-container">
                        <div className="D-content-container-frame" />
                        <h3 className="D-content-container-title">Design Consultancy</h3>
                        <p className="D-content-container-text">
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr
                            , sed invidunt ut labore et dolore
                            magna aliquyam erat, sed diam voluptua. At vero eos
                            et accusam et justo duo dolores et ea rebum.
                            Stet kasd gubergren, no sea takimata
                        </p>
                    </div>
                    <div className="D-content-container">
                        <div className="D-content-container-frame" />
                        <h3 className="D-content-container-title">Social Media</h3>
                        <p className="D-content-container-text">
                            Lorem ipsum dolor sit amet elitr
                            , sed diam nonumy eirmod tempor invidunt ut labore et dolore
                            et accusam et justo duo dolores et ea rebum.
                            Stet clita kasd gubergren, no sea takimata
                        </p>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Do
