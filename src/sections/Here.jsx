import React from 'react'
import '../styles/Here.scss'

import Logo from '../assets/logoRedondoPreto.png'

import { IoLogoFacebook, IoLogoInstagram, IoLogoWhatsapp } from "react-icons/io";

export const Here = () => {
    return (
        <section className="Here-wrapper">
            <div className="Here-container">
                <div className="Here-top">
                    <h3>
                        It's all about the humans behind a brand and those experiencing it, we're right there... In the middle.
                    </h3>
                </div>
                <div className="Here-bottom">
                    <div className="Here-navigation">
                        <div className="Here-logo">
                            <img src={Logo} alt="logo" />
                        </div>

                        <button>Contact</button>

                        <div className="Here-links">
                            <p>Main</p>
                            <p>Work</p>
                            <p>Cases</p>
                            <p>Contact</p>
                        </div>
                    </div>
                    <div className="Here-footer">
                        <p>&copy; Domenica.</p>
                        <div>
                            <IoLogoFacebook style={{margin: "0 .25em"}}/>
                            <IoLogoInstagram style={{margin: "0 .25em"}}/>
                            <IoLogoWhatsapp style={{margin: "0 .25em"}}/>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}
