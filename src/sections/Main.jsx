import React from 'react'
import '../styles/Main.scss'

import Logo from '../assets/logoRedondoVermelho.png'
import Hero from '../assets/hero2.jpg'

import { Tween } from 'react-gsap';

const Main = () => {
    return (
        <section className="M-wrapper">
            <div className="H-wrapper">
                <img src={Logo} alt="" />
            </div>
            <div className="I-wrapper">
                <img src={Hero} />
                <div className="I-background" />
            </div>
            <div className="T-wrapper">
                <Tween from={{ opacity: '0', x: '-40px' }} duration={2} stagger={1} ease="ease-out">
                    <h1>We are<br />Domenica.</h1>
                    <p>we believe in sharing<br />unique brand with the world<br />your idea in her prime</p>
                </Tween>
            </div>
            <div className="F-wrapper">
                <Tween from={{ x: '80px', opacity: '0' }} duration={2} ease="ease-out">
                    <h2>And We Do!</h2>
                </Tween>
            </div>
        </section>
    )
}

export default Main
